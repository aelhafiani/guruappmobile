/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Alert, BackHandler, Linking  } from 'react-native';
import { WebView } from 'react-native-webview';
import DeviceInfo from 'react-native-device-info'; 
import { Header } from 'react-native-elements';

type Props = {};
export default class App extends Component<Props> {
  WEBVIEW_REF = React.createRef();
 constructor(props) { 
   super(props);
   this.state = {
    canGoBack: false
   }
  
 } 
 componentDidMount() {
  BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
}

componentWillUnmount() {
  BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
}

 handleBackButton = () => {
  if (this.state.canGoBack) {
    this.WEBVIEW_REF.current.goBack();
    return true;
  }
};

onNavigationStateChange = (navState) => {
  let url = navState.url.split('/')
  console.log('url',url)
  this.setState({
    canGoBack: navState.canGoBack,
  });
if (url[2] == 'fr.airbnb.com' || url[2] == 'www.airbnb.com') { 
    this.WEBVIEW_REF.current.stopLoading();
    Linking.openURL(navState.url);
  }
};

onMessage = (event) => {
    const {title, message} = JSON.parse(event.nativeEvent.data)
    Alert.alert(
      title,
      message,
      [], 
      { cancelable: true }
    );
  }

  render() {
  

    return (
      <View style={styles.container}>
        <Header 
        containerStyle={{
          backgroundColor: '#000',
          paddingBottom:0,
          marginBottom:0,
          height:0
        }}
        />
        <WebView          
         
          source={{ uri: 'https://staging.guru4all.com' }}
          javaScriptEnabled={true}
          originWhitelist={['*']}
          allowFileAccess={true}
          userAgent={DeviceInfo.getUserAgent() + " - GURU "}
          onMessage={this.onMessage} 
          ref={this.WEBVIEW_REF}
          onNavigationStateChange={this.onNavigationStateChange}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});